JSONP
=====

### iTunes

https://www.apple.com/itunes/affiliates/resources/documentation/itunes-store-web-service-search-api.html

### ESPN (Will be removed on December 8, 2014)

1. Create an API key: http://developer.espn.com/member/register
2. Look at the various endpoints: http://developer.espn.com/docs
3. Sample JSONP endpoint: http://api.espn.com/v1/now/popular?apikey=YOUR_API_KEY&callback=sup

### Rotten Tomatoes
1. Create an API key: http://developer.rottentomatoes.com/member/register
2. Documentation and various endpoints: http://developer.rottentomatoes.com/docs
3. JSONP endpoint: http://api.rottentomatoes.com/api/public/v1.0.json?apikey=[your_api_key]

### YouTube
1. Create an API key: https://developers.google.com/youtube/registering_an_application
2. Documentation on getting started: https://developers.google.com/youtube/v3/getting-started
3. Sample JSONP endpoint: https://www.googleapis.com/youtube/v3/videos?id=7lCDEYXw3mM&key=YOUR_API_KEY&part=snippet,contentDetails,statistics,status

### Foursquare
1. Create an account to get client ID and client secret.
2. Look at various endpoints: https://developer.foursquare.com/docs/
3. Sample JSONP endpoint: https://api.foursquare.com/v2/venues/search?client_id=CLIENT_ID&client_secret=CLIENT_SECRET&v=20130815&ll=40.7,-74&query=sushi

### Instagram
1. Create an API key: http://instagram.com/developer/clients/manage/
2. Look at various endpoints: http://instagram.com/developer/endpoints/
3. Sample JSONP endpoint: https://instagram.com/oauth/authorize/?display=touch&client_id=[ClientID]&redirect_uri=[callbackuri]&response_type=token

[More JSONP endpoints](http://www.programmableweb.com/category/all/apis?data_format=21174)